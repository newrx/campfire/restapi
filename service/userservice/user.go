package userservice

import (
	"fmt"
	"time"
	"errors"
	"context"
	"bytes"

	"github.com/jmoiron/sqlx"
	identicon "github.com/dgryski/go-identicon"
	"github.com/google/uuid"

	"gitlab.com/newrx/campfire/api/service/database"
	"gitlab.com/newrx/campfire/api/service/s3"
	// "gitlab.com/newrx/campfire/api/service/sms"
)

var ErrUserNotFound = errors.New("failed to get user by id, user not found")

type User struct {
	ID          string          `json:"id" db:"id"`
	Username    string          `json:"username" db:"username"`
	Name        string          `json:"name" db:"name"`
	Bio string `json:"bio" db:"bio"`
	PhoneNumber string `json:"phoneNumber" db:"phone_number"`
	AvatarURL string `json:"avatarUrl" db:"-"`
	PasswordHash string `json:"-" db:"password_hash"`

	CreatedAt time.Time `json:"createdAt" db:"created_at"`
	LastModAt time.Time `json:"lastModAt" db:"last_mod_at"`
}

func CreateUser(db *database.Database, storage *s3.Bucket, user User) (User, error) {
	createUserQuery := `
		INSERT INTO users (
			id,
			username,
			name,
			bio,
			phone_number,
			created_at,
			last_mod_at
		) VALUES (
			:id,
			:username,
			:name,
			:bio,
			:phone_number,
			:created_at,
			:last_mod_at
		)`

	user.ID = uuid.New().String()
	user.CreatedAt = time.Now()
	user.LastModAt = time.Now()
	user.AvatarURL = fmt.Sprintf("/api/users/%s/avatar", user.ID)

	err := db.Update(context.Background(), func(tx *sqlx.Tx) error {
		err := CreateAvatarById(storage, user)
		if err != nil {
			return err
		}

		_, err = tx.NamedExec(createUserQuery, user)

		return err
	})
	if err != nil {
		return User{}, err
	}

	return user, nil
}

func GetUserByUsername(db *database.Database, username string) (User, error) {
	getUserQuery :=
	`SELECT
		id,
		username,
		name,
		bio,
		phone_number,
		created_at,
		last_mod_at
	 FROM users
	 WHERE username = $1
	`

	user := User{}
	err := db.View(context.Background(), func(tx *sqlx.Tx) error {
		return tx.Get(&user, getUserQuery, username)
	})

	user.AvatarURL = fmt.Sprintf("/api/users/%s/avatar", user.ID)
	return user, err
}

func GetUserByID(db *database.Database, id string) (User, error) {
	getUserQuery :=
	`SELECT
		id,
		username,
		name,
		bio,
		phone_number,
		created_at,
		last_mod_at
	 FROM users
	 WHERE id = $1
	`

	user := User{}
	err := db.View(context.Background(), func(tx *sqlx.Tx) error {
		return tx.Get(&user, getUserQuery, id)
	})

	user.AvatarURL = fmt.Sprintf("/api/users/%s/avatar", user.ID)
	return user, err
}

func DeleteUserByID(db *database.Database, id string) error {
	return db.Update(context.Background(), func(tx *sqlx.Tx) error {
		_, err := tx.Exec("DELETE FROM users WHERE id = $1", id)
		return err
	})
}

func CreateAvatarById(storage *s3.Bucket, user User) error {
	key := []byte{0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF}
	icon := identicon.New7x7(key)

	return storage.Upload(
		fmt.Sprintf("user-%s-avatar", user.ID),
		bytes.NewBuffer(icon.Render([]byte(user.Username)),
	))
}

func GetAvatarByID(storage *s3.Bucket, id string) (s3.File, error) {
	return storage.Download(fmt.Sprintf("user-%s-avatar", id))
}
