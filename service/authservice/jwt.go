package authservice

import (
  "time"
  "crypto/rsa"

  jwt "github.com/dgrijalva/jwt-go"
  "github.com/google/uuid"
)

type JWTClaims struct {
    SubjectType string `json:"sty"`
    SubjectRoles []string `json:"rls"`
    jwt.StandardClaims
}

type AuthService struct{
  SigningKey *rsa.PrivateKey
  Issuer string
  Audience string
}

func New(issuer, audience, rsaKey string) (*AuthService, error) {
  privateKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(rsaKey))
  if err != nil {
    return nil, err
  }

  return &AuthService{
    SigningKey: privateKey,
    Issuer: issuer,
    Audience: audience,
  }, nil
}

func (a *AuthService) CreateUserJWT(userID string) (string, error) {
  token := jwt.NewWithClaims(
    jwt.SigningMethodRS256,
    JWTClaims{
      SubjectType: "user",
      SubjectRoles: []string{"user"},
      StandardClaims: jwt.StandardClaims{
        Audience: a.Audience, // TODO determine value
        ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
        Id: uuid.New().String(),
        IssuedAt: time.Now().Unix(), //uint64
        Issuer:    a.Issuer,
        NotBefore: time.Now().Unix(),
        Subject: userID,
      },
  })

  return token.SignedString(a.SigningKey)
}

func (a *AuthService) CreateSingleRoleJWT(phoneNumber string) (string, error) {
  token := jwt.NewWithClaims(
    jwt.SigningMethodRS256,
    JWTClaims{
      SubjectType: "user",
      SubjectRoles: []string{"singlerole_user"},
      StandardClaims: jwt.StandardClaims{
          Audience: a.Audience, // TODO determine value
          ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
          Id: "user-role-singleperm-sms-signuptoken",
          IssuedAt: time.Now().Unix(), //uint64
          Issuer:    a.Issuer,
          NotBefore: time.Now().Unix(),
          Subject: phoneNumber,
      },
  })

  return token.SignedString(a.SigningKey)
}

func (a *AuthService) CreateServiceJWT(apiKey string) (string, error) {
  token := jwt.NewWithClaims(
    jwt.SigningMethodRS256,
    JWTClaims{
      SubjectType: "service",
      SubjectRoles: []string{"service"},
      StandardClaims: jwt.StandardClaims{
          Audience: a.Audience, // TODO determine value
          ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
          Id: uuid.New().String(),
          IssuedAt: time.Now().Unix(), //uint64
          Issuer:    a.Issuer,
          NotBefore: time.Now().Unix(),
          Subject: apiKey,
      },
  })

  return token.SignedString(a.SigningKey)
}
