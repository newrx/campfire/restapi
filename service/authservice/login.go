package authservice

// import "golang.org/x/crypto/bcrypt"
// hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
// err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)

import (
    "net/http"
    "errors"
    "context"

    "golang.org/x/crypto/bcrypt"

  	"gitlab.com/newrx/campfire/api/service/database"
    "gitlab.com/newrx/campfire/api/service/userservice"
)

const sessionContextKey = "x-campfire-api-session-store"

func (a *AuthService) RetrieveLoginJWT(db *database.Database, username, password string) (string, error) {
  user, err := userservice.GetUserByUsername(db, username)
  if err != nil {
    return "", err
  }

  err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(password))
  if err != nil {
    return "", err
  }

  return a.CreateUserJWT(user.ID)
}

type Session struct{
  SubjectID string
  SubjectType string
  SubjectRoles []string
}

func WithSession(req *http.Request, session Session) *http.Request {
    return req.WithContext(context.WithValue(req.Context(), sessionContextKey, session))
}

func FromSession(req *http.Request) (Session, error) {
  session, ok := req.Context().Value(sessionContextKey).(Session)
  if !ok {
    return Session{}, errors.New("failed to retrieve session, session is invalid or missing from context")
  }

  return session, nil
}
