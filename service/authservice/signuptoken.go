package authservice

import (
	"fmt"
	"time"
	"context"

	"github.com/jmoiron/sqlx"

	"gitlab.com/newrx/campfire/api/service/database"
	"gitlab.com/newrx/campfire/api/service/sms"
)

const verificationCodeMsg = `Here's your Campfire verification code: %s`

type SignupToken struct{
	PhoneNumber string `db:"phone_number"`
	CreatedAt time.Time `db:"created_at"`
	VerificationCode string `db:"verification_code"`
	Verified bool `db:"verified"`
	JWT string `db:"jwt"`
}

func (a *AuthService) CreateSignupToken(db *database.Database, messenger sms.Messenger, phoneNumber string) error {
	jwt, err := a.CreateSingleRoleJWT(phoneNumber)
	if err != nil {
		return err
	}

	signupToken := SignupToken{
		PhoneNumber: phoneNumber,
		CreatedAt: time.Now(),
		VerificationCode: GenerateRandomCode(6),
		Verified: false,
		JWT: jwt,
	}

	return db.Update(context.Background(), func(tx *sqlx.Tx) error {
		signupSql := `
		INSERT INTO signuptokens (
			phone_number,
			created_at,
			verification_code,
			verified,
			jwt
		) VALUES (
			:phone_number,
			:created_at,
			:verification_code,
			:verified,
			:jwt
		)
		`

		_, err := tx.NamedExec(signupSql, signupToken)
		if err != nil {
			return err
		}

		return messenger.Send(
			"Campfire Phone Verification",
			phoneNumber,
			fmt.Sprintf(verificationCodeMsg, signupToken.VerificationCode),
		)
	})
}

func (a *AuthService) RetrieveSignupJWT(db *database.Database, phoneNumber, verificationCode string) (string, error) {
	signupToken := SignupToken{}
	err := db.View(context.Background(), func(tx *sqlx.Tx) error {
		return tx.Get(
			&signupToken,
			"SELECT * FROM signuptokens WHERE phone_number = $1 AND verification_code = $2 AND verified = false",
			phoneNumber,
			verificationCode,
		)
	})
	if err != nil {
		return "", err
	}

	return signupToken.JWT, db.Update(context.Background(), func(tx *sqlx.Tx) error {
		_, err := tx.Exec(
			"DELETE FROM signuptokens WHERE phone_number = $1 AND verification_code = $2 AND verified = false",
			phoneNumber,
			verificationCode,
		)

		return err
	})
}
