package authservice

type Action string

const (
  CREATE Action = "CREATE"
  READ Action = "READ"
  UPDATE Action = "UPDATE"
  DELETE Action = "DELETE"
)

type Permission struct{
  Name string `db:"permission_name"`
  ResourceName string `db:"resource_name"`
  Action `db:"action_name"`
}
