package authservice

import (
	"time"
	"math/rand"
)

// must be numbers only in order to be detected as a code by Android
const sample = "0123456789"

func init() {
	rand.Seed(time.Now().UnixNano())
}

// TODO should rename to pin
func GenerateRandomCode(length int) string {
	code := ""
	for i := 0; i < length; i++ {
		code += string(sample[rand.Intn(len(sample))])
	}

	return code
}
