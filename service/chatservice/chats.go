package chatservice

import (
  "time"
  "context"

  "github.com/jmoiron/sqlx"
  "github.com/google/uuid"

  "gitlab.com/newrx/campfire/api/service/database"
  "gitlab.com/newrx/campfire/api/httputils"
)

type Chat struct{
  ID string `json:"id" db:"id"`
  Title string `json:"title" db:"title"`
  CreatedAt time.Time `json:"createdAt" db:"created_at"`
  Owner string `json:"owner" db:"owner_user_id"`
}

func CreateChat(db *database.Database, chat Chat) (Chat, error) {
  chat.ID = uuid.New().String()
  chat.CreatedAt = time.Now()

  err := db.Update(context.Background(), func(tx *sqlx.Tx) error {
    _, err := tx.NamedExec("INSERT INTO chats (id, title, created_at) VALUES (:id, :title, :created_at)", chat)
    return err
  })

  return chat, err
}

func SearchChats(db *database.Database, page httputils.Page, query interface{}) (httputils.PageData, error) {
  countQueryStr := `
  SELECT COUNT(*) FROM chats
  `

  queryStr := `
  SELECT *.c, col FROM chats c LEFT JOIN chats_owners_lookup col ON c.id = col.chat_id LIMIT $1 OFFSET $2
  `

  var count uint64
  chats := make([]Chat, 0)

  err := db.View(context.Background(), func(tx *sqlx.Tx) error {
    err := tx.Get(&count, countQueryStr)
    if err != nil {
      return err
    }

    offset := page.Offset + ((page.Page - 1) * page.PageSize)
    rows, err := tx.Query(queryStr, page.PageSize, offset)
    if err != nil {
      return err
    }

    // iterate over each row
    for rows.Next() {
      chat := Chat{}
      err = rows.Scan(&chat.ID, &chat.Title, &chat.CreatedAt)
      if err != nil {
        return err
      }

      chats = append(chats, chat)
    }

    return nil
  })

  return httputils.PageData{
    PageMeta: httputils.PageMeta{
      Page: page,
      TotalElements: count,
      TotalPages: page.CalcTotalPages(count),
    },
    Data: chats,
  }, err
}

func DeleteChatByID(db *database.Database, chatId string) error {
  deleteQuery := `
  DELETE FROM messages m
    LEFT JOIN chats_messages_lookup cml ON cml.message_id = m.id
  WHERE cml.chat_id = $1
  `

  return db.Update(context.Background(), func(tx *sqlx.Tx) error {
    _, err := tx.Exec(deleteQuery, chatId)
    if err != nil {
      return err
    }

    _, err = tx.Exec("DELETE FROM chats_messages_lookup WHERE chat_id = $1", chatId)
    if err != nil {
      return err
    }

    _, err = tx.Exec("DELETE FROM chats WHERE id = $1", chatId)
    return err
  })
}
