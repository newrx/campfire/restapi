package chatservice

import (
  "time"
  "context"
  "errors"

  "github.com/jmoiron/sqlx"
  "github.com/google/uuid"

  "gitlab.com/newrx/campfire/api/service/database"
  "gitlab.com/newrx/campfire/api/httputils"
)

var (
  ErrNotFound = errors.New("failed to find record, doesn't exist")
)

type Message struct{
  ID string `json:"id" db:"id"`
  ContentType string `json:"contentType" db:"content_type"`
  Body string `json:"body" db:"body"`
  SentAt time.Time `json:"sentAt" db:"sent_at"`
}

func CreateMessageByChatID(db *database.Database, chatId string, message Message) (Message, error) {
  exists := false
  err := db.View(context.Background(), func(tx *sqlx.Tx) error {
    return tx.Get(&exists, "SELECT EXISTS(SELECT 1 from chats where id = $1)", chatId)
  })
  if err != nil {
    return Message{}, err
  }

  if !exists {
    return Message{}, ErrNotFound
  }

  insertStr := `
  INSERT INTO messages (
    id,
    content_type,
    body,
    sent_at
  ) VALUES (
    :id,
    :content_type,
    :body,
    :sent_at
  )
  `

  message.ID = uuid.New().String()
  err = db.Update(context.Background(), func(tx *sqlx.Tx) error {
    _, err := tx.NamedExec(insertStr, message)
    if err != nil {
      return err
    }

    _, err = tx.Exec(
      "INSERT INTO chats_messages_lookup (chat_id, message_id) VALUES ($1, $2)",
      chatId,
      message.ID,
    )
    return err
  })

  return message, err
}

func GetAllMessagesByChatID(db *database.Database, page httputils.Page, chatId string) (httputils.PageData, error) {
  countQueryStr := `
  SELECT COUNT(m.*) FROM messages m
    LEFT JOIN chats_messages_lookup cml ON cml.message_id = m.id
  WHERE cml.chat_id = $1
  `

  queryStr := `
  SELECT m.* FROM messages m
    LEFT JOIN chats_messages_lookup cml ON cml.message_id = m.id
  WHERE cml.chat_id = $1
  ORDER BY m.sent_at
  LIMIT $2
  OFFSET $3
  `

  var count uint64
  messages := make([]Message, 0)

  err := db.View(context.Background(), func(tx *sqlx.Tx) error {
    err := tx.Get(&count, countQueryStr, chatId)
    if err != nil {
      return err
    }

    offset := page.Offset + ((page.Page - 1) * page.PageSize)
    rows, err := tx.Query(queryStr, chatId, page.PageSize, offset)
    if err != nil {
      return err
    }

    // iterate over each row
    for rows.Next() {
      message := Message{}
      err = rows.Scan(&message.ID, &message.ContentType, &message.Body, &message.SentAt)
      if err != nil {
        return err
      }

      messages = append(messages, message)
    }

    return nil
  })

  return httputils.PageData{
      PageMeta: httputils.PageMeta{
        Page: page,
        TotalElements: count,
        TotalPages: page.CalcTotalPages(count),
      },
      Data: messages,
  }, err
}
