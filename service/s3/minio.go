package s3

import (
	"io"
	"errors"

	minio "github.com/minio/minio-go"
)

const defaultBucket = "campfire-api.v1"
const defaultRegion = "us-east-1"

var ErrNotFound = errors.New("seeker can't seek")

type Bucket struct {
	client *minio.Client
}

type File interface {
	io.Reader
	io.Seeker
	io.ReaderAt
	io.Closer
}

type Config struct {
	Host, APIKey, Secret string
}

func New(conf Config) (*Bucket, error) {
	minioClient, err := minio.New(conf.Host, conf.APIKey, conf.Secret, false)
	if err != nil {
		return nil, err
	}

	ok, err := minioClient.BucketExists(defaultBucket)
	if err != nil {
		return nil, err
	}

	if !ok {
		err = minioClient.MakeBucket(defaultBucket, defaultRegion)
		if err != nil {
			return nil, err
		}
	}

	return &Bucket{
		client: minioClient,
	}, nil
}

func (f *Bucket) Upload(filepath string, contents io.Reader) error {
	_, err := f.client.PutObject(
		defaultBucket,
		filepath,
		contents,
		-1,
		minio.PutObjectOptions{},
	)

	return err
}

func (f *Bucket) Download(filepath string) (File, error) {
	return f.client.GetObject(defaultBucket, filepath, minio.GetObjectOptions{})
}

func (f *Bucket) Delete(filepath string) error {
	return f.client.RemoveObject(defaultBucket, filepath)
}
