package log

import (
	"io"
	"os"
	"time"

	"github.com/rs/zerolog"
)

type Logger struct {
	zerolog.Logger
}

type Level string

func (l Level) ZerologLevel() zerolog.Level {
	switch l {
	case LevelInfo:
		return zerolog.InfoLevel
	case LevelDebug:
		return zerolog.DebugLevel
	case LevelWarn:
		return zerolog.WarnLevel
	case LevelError:
		return zerolog.ErrorLevel
	}

	// default to the lowest level
	return zerolog.DebugLevel
}

const (
	LevelInfo  Level = "info"
	LevelDebug Level = "debug"
	LevelWarn  Level = "warn"
	LevelError Level = "error"
)

func New(pretty bool, module string, level Level) Logger {
	var output io.Writer = os.Stdout
	if pretty {
		output = zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	}

	return Logger{
		zerolog.New(output).With().
			Str("mod", module).
			Timestamp().
			Logger().Level(zerolog.Level(level.ZerologLevel()))}
}
