package geospatial

type Point struct {
	Lat float32
	Lng float32
}
