package geospatial

import (
	"github.com/gomodule/redigo/redis"
)

type Database struct {
	client redis.Conn
}

type Config struct {
	Host string
	User     string
	Password string
	Database int
}

func Dial(conf Config) (*Database, error) {
	c, err := redis.Dial(
		"tcp", conf.Host,
		redis.DialDatabase(conf.Database),
	)
	if err != nil {
		return nil, err
	}

	return &Database{c}, nil
}

func (d *Database) Ping() error {
	_, err := d.client.Do("PING")
	return err
}

func (d *Database) Close() error {
	return d.client.Close()
}

func (d *Database) Insert() error {
	return nil
}
