package geospatial

import "errors"

var ErrNotFound = errors.New("object not found")

type Property struct {
	ID string

	// left to right
	Bounds []Point
}

func (d *Database) CreateProperty(prop Property) error {
	// SET props house1 BOUNDS 33.7840 -112.1520 33.7848 -112.1512
	return nil
}

func (d *Database) PropertyExists(id string) error {
	return nil
}
