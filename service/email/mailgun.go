package sms

import (
	"fmt"
	"net/http"
	"net/url"
)

type Sender string

type Messenger struct {
	apiKey       string
	senderDomain string
}

type Config struct {
	APIKey, SenderDomain string
}

func New(conf Config) Messenger {
	return Messenger{
		senderDomain: conf.SenderDomain,
		apiKey:       conf.APIKey,
	}
}

func (m Messenger) GenerateSender(name, emailUser string) Sender {
	return Sender(fmt.Sprintf("%s <%s@%s>", name, emailUser, m.senderDomain))
}

func (m Messenger) Send(subject, to, text string, from Sender) error {
	// key-3ax6xnjp29jd6fds4gc373sgvjxteol0
	fullURL := fmt.Sprintf(
		"https://api:%s@api.mailgun.net/v3/samples.mailgun.org/messages",
		m.apiKey,
	)

	_, err := http.PostForm(fullURL, url.Values{
		"from":    {string(from)},
		"to":      {to},
		"subject": {subject},
		"text":    {text},
	})

	if err != nil {
		return err
	}

	return err
}

func (m Messenger) SendHTML(subject, from, to, html string) error {
	// key-3ax6xnjp29jd6fds4gc373sgvjxteol0
	fullURL := fmt.Sprintf(
		"https://api:%s@api.mailgun.net/v3/samples.mailgun.org/messages",
		m.apiKey,
	)

	_, err := http.PostForm(fullURL, url.Values{
		"from":    {from},
		"to":      {to},
		"subject": {subject},
		"html":    {html},
	})

	if err != nil {
		return err
	}

	return err
}
