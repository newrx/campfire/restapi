package sms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type Messenger struct {
	apiKey       string
	apiSecret    string
	senderNumber string
}

type Config struct {
	APIKey, Secret, SenderNumber string
}

func New(conf Config) Messenger {
	return Messenger{
		senderNumber: conf.SenderNumber,
		apiKey:       conf.APIKey,
		apiSecret:    conf.Secret,
	}
}

type NexmoSendResponse struct{
	Messages []struct{
		Status string `json:"status"`
	} `json:"messages"`
}

func (m Messenger) Send(title, to, message string) error {
	buff := bytes.Buffer{}
	err := json.NewEncoder(&buff).Encode(map[string]interface{}{
		"api_key":    m.apiKey,
		"api_secret": m.apiSecret,
		"to":         fmt.Sprintf("1%s", to),
		"from":       m.senderNumber,
		"text":       message,
		"title":      title,
	})
	if err != nil {
		return err
	}

	res, err := http.Post("https://rest.nexmo.com/sms/json", "application/json", &buff)
	if err != nil {
		return err
	}

	nexmoRes := NexmoSendResponse{}
	err = json.NewDecoder(res.Body).Decode(&nexmoRes)
	if err != nil {
		return err
	}

	if nexmoRes.Messages[0].Status != "0" {
		return fmt.Errorf("failed to complete sms send, status: %s", nexmoRes.Messages[0].Status)
	}

	return err
}
