package sms

import "fmt"

type PhoneNumber [10]uint8

func (p PhoneNumber) String() string {
	return fmt.Sprintf("+1 (000)-000-0000 %s", "")
}
