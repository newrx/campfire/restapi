package database

import (
	"context"
	"database/sql"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Config struct {
	User, Password, Host, Port, DatabaseName, MigrationDir string
}

type Database struct {
	client *sqlx.DB
	migrationDir string
}

func Dial(conf Config) (*Database, error) {
	url := fmt.Sprintf(
		"user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
		conf.User,
		conf.Password,
		conf.Host,
		conf.Port,
		conf.DatabaseName,
	)
	db, err := sql.Open("postgres", url)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS db_version (
			id VARCHAR(1),
			version bigint,
			hash VARCHAR(256),
			file VARCHAR(256),
			last_run TIMESTAMPTZ,
			complete BOOLEAN
		);
	`)
	if err != nil {
		return nil, err
	}

	d := &Database{sqlx.NewDb(db, "postgres"), conf.MigrationDir}
	_, err = d.GetCurrentMigration()
	if err != nil {
		if err == sql.ErrNoRows {
			_, err := db.Exec(`
				INSERT INTO db_version
				(id, version, hash, file, last_run, complete) VALUES
				('1', 0, '', '', NOW(), true);
			`)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}

	return d, nil
}

func (d *Database) Ping() error {
	return d.client.Ping()
}

func (d *Database) Close() error {
	return d.client.Close()
}

func (d *Database) exec(ctx context.Context, callback func(*sqlx.Tx) error, readOnly bool) error {
	tx, err := d.client.BeginTxx(ctx, &sql.TxOptions{Isolation: sql.LevelSerializable, ReadOnly: readOnly})
	if err != nil {
		return err
	}

	err = callback(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (d *Database) View(ctx context.Context, callback func(*sqlx.Tx) error) error {
	return d.exec(ctx, callback, true)
}

func (d *Database) Update(ctx context.Context, callback func(*sqlx.Tx) error) error {
	return d.exec(ctx, callback, false)
}

func (d *Database) ExecFile(filepath string) error {
	bytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}

	// split on the semicolon delimiter
	blocks := strings.Split(string(bytes), ";")

	return d.Update(context.Background(), func(tx *sqlx.Tx) error {
		for i, block := range blocks {
			_, err := tx.Exec(block)
			if err != nil {
				return fmt.Errorf("failed to execute block %d of sql file: %s", i, err.Error())
			}
		}

		return nil
	})
}
