package httputils

import (
  "net/http"
  "net/url"
  "strconv"
)

// Page is used to storing pagination values passed as query params with a
// request, and to paginate data returned by the service layer.
type Page struct{
  Page uint64 `json:"page"`
  PageSize uint64 `json:"pageSize"`
  Offset uint64 `json:"offset"`
}

func (page Page) CalcTotalPages(numElements uint64) uint64 {
  pages := numElements / page.PageSize
  hasTrail := numElements % page.PageSize > 0
  if hasTrail {
    return pages + 1
  }

  return pages
}

// PageMeta is used with paginated return data in order to give receipt of a
// paginated dataset, as well as provide information for rendering a pagination
// controller.
type PageMeta struct{
  Page
  TotalElements uint64 `json:"totalElements"`
  TotalPages uint64 `json:"totalPages"`
}

// PageData is used as the paginated return data, containing both the data itself,
// and the metadata for future paginated requests.
type PageData struct{
  PageMeta PageMeta `json:"meta"`
  Data interface{} `json:"data"`
}

func AcceptPagination(req *http.Request) (Page, bool) {
  queryParams, err := url.ParseQuery(req.URL.RawQuery)
  if err != nil {
    return Page{}, false
  }

  pageArr, ok := queryParams["page"]
  if !ok {
    return Page{}, false
  }

  pageNum, err := strconv.ParseUint(pageArr[0], 10, 64)
  if err != nil {
    return Page{}, false
  }

  if pageNum < 1 {
    return Page{}, false
  }

  var pageSizeNum uint64
  pageSizeArr, ok := queryParams["pageSize"]
  if !ok {
    pageSizeNum = 20
  } else {
    pageSizeNum, err = strconv.ParseUint(pageSizeArr[0], 10, 64)
    if err != nil {
      return Page{}, false
    }
  }

  var offsetSizeNum uint64
  offsetArr, ok := queryParams["offset"]
  if !ok {
    offsetSizeNum = 0
  } else {
    offsetSizeNum, err = strconv.ParseUint(offsetArr[0], 10, 64)
    if err != nil {
      return Page{}, false
    }
  }

  return Page{
    Page: pageNum,
    PageSize: pageSizeNum,
    Offset: offsetSizeNum,
  }, true
}
