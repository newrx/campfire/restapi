package httputils

import (
  "fmt"
  "net/http"
  "encoding/json"
)

func AcceptJSON(req *http.Request, res http.ResponseWriter, mapping interface{}) bool {
  if req.Header.Get("Content-Type") != "application/json" {
    http.Error(
      res,
      "failed to process: request body and content type must be 'application/json'",
      http.StatusBadRequest,
    )
    return false
  }

  err := json.NewDecoder(req.Body).Decode(mapping)
  if err != nil {
    http.Error(
      res,
      fmt.Sprintf("failed to process request, invalid request body: %s", err.Error()),
      http.StatusBadRequest,
    )
    return false
  }

  return true
}

func ReturnJSON(res http.ResponseWriter, obj interface{}) bool {
  res.Header().Add("Content-Type", "application/json")
  err := json.NewEncoder(res).Encode(obj)
  if err != nil {
    http.Error(
      res,
      fmt.Sprintf("failed to complete request, could not return response: %s", err.Error()),
      http.StatusInternalServerError,
    )
    return false
  }

  return true
}

func ReturnCustomStatusJSON(res http.ResponseWriter, obj interface{}, status int) bool {
  res.Header().Add("Content-Type", "application/json")
  bytes, err := json.Marshal(obj)
  if err != nil {
    http.Error(
      res,
      fmt.Sprintf("failed to complete request, could not return response: %s", err.Error()),
      http.StatusInternalServerError,
    )
    return false
  }

  res.WriteHeader(status)
  res.Write(bytes)

  return true
}
