package httputils

import (
  "net/http"
  "io"
)

func ReturnReader(res http.ResponseWriter, body io.Reader) bool {
  res.Header().Add("Content-Type", "application/octet-stream")
  _, err := io.Copy(res, body)
  if err != nil {
    http.Error(res, err.Error(), 500)
    return false
  }

  return true
}

func ReturnCustomStatusReader(res http.ResponseWriter, body io.Reader, statusCode int) bool {
  res.Header().Add("Content-Type", "application/octet-stream")
  _, err := io.Copy(res, body)
  if err != nil {
    http.Error(res, err.Error(), 500)
    return false
  }

  res.WriteHeader(statusCode)
  return true
}
