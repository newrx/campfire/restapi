# provides secrets from .env
include .env

build:
	-rm -rf bin
	mkdir -p bin
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o bin/api main.go

rsakeys:
	ssh-keygen -t rsa -b 2048 -m PEM -f .rsa_signing.key -N ''
	# openssl rsa -in jwt.key -pubout -outform PEM -out jwt.key.pub

migrations:
	goose postgres "user=postgres dbname=postgres sslmode=disable" ./migrations up

run:
	CAPI_GEO_DB_USER=campfire \
	CAPI_GEO_DB_PASSWORD=campfire \
	CAPI_GEO_DB_HOST=localhost:9851 \
	CAPI_POSTGRES_USER=campfire \
	CAPI_POSTGRES_PASSWORD=campfire \
	CAPI_POSTGRES_HOST=localhost \
	CAPI_POSTGRES_PORT=5432 \
	CAPI_POSTGRES_MIGRATIONDIR=migrations \
	CAPI_S3_HOST=localhost:9000 \
	CAPI_S3_APIKEY=campfire \
	CAPI_S3_SECRET=campfire \
	CAPI_NEXMO_SENDERNUMBER=12034418080 \
	CAPI_NEXMO_APIKEY=${NEXMO_APIKEY} \
	CAPI_NEXMO_SECRET=${NEXMO_SECRET} \
	CAPI_SERVICE_ENV=localdev \
	CAPI_SERVICE_SIGNINGKEY=.rsa_signing.key \
	go run main.go

test:
	CGO_ENABLED=0 go test -v ./... -cover

int-test:
	CGO_ENABLED=0 go test -v -tags=int_test ./..

docker: build
	docker build -t campfire/api:latest .

.PHONY: build dev docker int-test
