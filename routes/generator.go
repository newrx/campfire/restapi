package routes

import (
	"net/http"

	goji "goji.io"
	"goji.io/pat"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/newrx/campfire/api/controller"
	"gitlab.com/newrx/campfire/api/middlewares"
	"gitlab.com/newrx/campfire/api/service/database"
	"gitlab.com/newrx/campfire/api/service/geospatial"
	"gitlab.com/newrx/campfire/api/service/log"
	"gitlab.com/newrx/campfire/api/service/s3"
	"gitlab.com/newrx/campfire/api/service/sms"
	"gitlab.com/newrx/campfire/api/service/authservice"
)

func Generate(auth *authservice.AuthService, db *database.Database, geodb *geospatial.Database, bucket *s3.Bucket, messenger sms.Messenger, logger log.Logger) http.Handler {
	api := goji.SubMux()
	api.HandleFunc(pat.Get("/service/health"), controller.HealthCheck(db))
	api.Handle(pat.New("/service/metrics"), promhttp.Handler())

	api.HandleFunc(pat.Post("/signuptokens/:phoneNumber"), controller.CreateSignupToken(auth, db, messenger, logger))
	api.HandleFunc(pat.Get("/signuptokens/:phoneNumber/code"), controller.GetSignupToken(auth, db, logger))

	api.HandleFunc(pat.Post("/authtokens/user"), controller.CreateUserAuthToken(db, logger))
	api.HandleFunc(pat.Get("/authtokens/status"), controller.ValidateAuthToken(db, logger))

	api.HandleFunc(pat.Post("/users"), controller.CreateUser(db, bucket, logger))
	api.HandleFunc(pat.Get("/users/:id"), controller.GetUserByID(db, logger))
	api.HandleFunc(pat.Delete("/users/:id"), controller.DeleteUserByID(db, logger))

	api.HandleFunc(pat.Post("/users/:id/avatar"), controller.UploadUserAvatar(db, bucket, logger))
	api.HandleFunc(pat.Get("/users/:id/avatar"), controller.DownloadUserAvatar(db, bucket, logger))

	api.HandleFunc(pat.Put("/users/:id/preferences"), controller.UpdateUserPreferences(db, logger))
	api.HandleFunc(pat.Put("/users/:id/location"), controller.UpdateUserLocation(db, logger))

	api.HandleFunc(pat.Post("/chats"), controller.CreateChat(db, logger))
	api.HandleFunc(pat.Get("/chats"), controller.SearchChats(db, logger))
	api.HandleFunc(pat.Delete("/chats/:id"), controller.DeleteChatByID(db, logger))

	api.HandleFunc(pat.Post("/chats/:id/messages"), controller.CreateMessageByChatID(db, logger))
	api.HandleFunc(pat.Get("/chats/:id/messages"), controller.GetAllChatMessages(db, logger))
	api.HandleFunc(pat.Delete("/chats/:chatId/messages/:messageId"), controller.DeleteMessageByID(db, logger))

	mux := goji.NewMux()
	mux.Use(middlewares.RecordTrip(logger))
	mux.Handle(pat.New("/api/*"), api)

	return mux
}
