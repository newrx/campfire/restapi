package controller

import (
  "net/http"

  "goji.io/pat"

  "gitlab.com/newrx/campfire/api/service/log"
  "gitlab.com/newrx/campfire/api/service/database"
  "gitlab.com/newrx/campfire/api/service/s3"
  "gitlab.com/newrx/campfire/api/service/userservice"
  "gitlab.com/newrx/campfire/api/httputils"
)

func CreateUser(db *database.Database, bucket *s3.Bucket, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    user := userservice.User{}
    ok := httputils.AcceptJSON(req, res, &user)
    if !ok {
      return
    }

    user, err := userservice.CreateUser(db, bucket, user)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

    httputils.ReturnCustomStatusJSON(res, user, http.StatusOK)
	})
}

func GetUserByID(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		id := pat.Param(req, "id")
    user, err := userservice.GetUserByID(db, id)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

    httputils.ReturnJSON(res, user)
	})
}

func DeleteUserByID(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    id := pat.Param(req, "id")
    err := userservice.DeleteUserByID(db, id)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }
	})
}
