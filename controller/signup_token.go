package controller

import (
  "net/http"

  "goji.io/pat"

  "gitlab.com/newrx/campfire/api/service/log"
  "gitlab.com/newrx/campfire/api/service/database"
  "gitlab.com/newrx/campfire/api/service/sms"
  "gitlab.com/newrx/campfire/api/service/authservice"
)

func CreateSignupToken(auth *authservice.AuthService, db *database.Database, messenger sms.Messenger, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    phoneNumber := pat.Param(req, "phoneNumber")
    err := auth.CreateSignupToken(db, messenger, phoneNumber)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

		res.WriteHeader(http.StatusOK)
	})
}

func GetSignupToken(auth *authservice.AuthService, db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    phoneNumber := pat.Param(req, "phoneNumber")
    key, code, ok := req.BasicAuth()
    if !ok {
      http.Error(res, "failed to process: request must include verification code", http.StatusBadRequest)
      return
    }

    if key != "verificationCode" {
      http.Error(res, "failed to process: invalid verification code format", http.StatusBadRequest)
      return
    }

    jwt, err := auth.RetrieveSignupJWT(db, phoneNumber, code)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

    http.SetCookie(res, &http.Cookie{
      Name: "x-campfire-api-auth",
      Value: jwt,
      HttpOnly: true,
      Path: "/api/*",
    })
	})
}
