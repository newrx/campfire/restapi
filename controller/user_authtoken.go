package controller

import (
  "net/http"

  "gitlab.com/newrx/campfire/api/service/log"
  "gitlab.com/newrx/campfire/api/service/database"
)

func CreateUserAuthToken(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
	})
}
