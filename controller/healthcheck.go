package controller

import (
	"net/http"

	"gitlab.com/newrx/campfire/api/service/database"
)

// HealthCheck returns the application configuration and attempts to ping the database,
// returning a internal server error if unsuccessful.
func HealthCheck(db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		err := db.Ping()
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}

		res.WriteHeader(http.StatusOK)
	})
}
