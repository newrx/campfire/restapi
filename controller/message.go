package controller

import (
  "net/http"

  "goji.io/pat"

  "gitlab.com/newrx/campfire/api/httputils"
  "gitlab.com/newrx/campfire/api/service/log"
  "gitlab.com/newrx/campfire/api/service/database"
  "gitlab.com/newrx/campfire/api/service/chatservice"
)

func CreateMessageByChatID(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    chatId := pat.Param(req, "id")
		message := chatservice.Message{}

    ok := httputils.AcceptJSON(req, res, &message)
    if !ok {
      return
    }

    message, err := chatservice.CreateMessageByChatID(db, chatId, message)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

    httputils.ReturnCustomStatusJSON(res, message, http.StatusCreated)
	})
}

func GetAllChatMessages(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    chatId := pat.Param(req, "id")
    page, ok := httputils.AcceptPagination(req)
    if !ok {
      page.Page = 1
      page.Offset = 0
      page.PageSize = 20
    }

    messages, err := chatservice.GetAllMessagesByChatID(db, page, chatId)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

		httputils.ReturnJSON(res, messages)
	})
}

func DeleteMessageByID(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
	})
}
