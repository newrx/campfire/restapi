package controller

import (
  "net/http"
  // "net/url"

  "goji.io/pat"

  "gitlab.com/newrx/campfire/api/httputils"
  "gitlab.com/newrx/campfire/api/service/log"
  "gitlab.com/newrx/campfire/api/service/database"
  "gitlab.com/newrx/campfire/api/service/chatservice"
)

func CreateChat(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    chat := chatservice.Chat{}
    ok := httputils.AcceptJSON(req, res, &chat)
    if !ok {
      return
    }

    chat, err := chatservice.CreateChat(db, chat)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

    httputils.ReturnCustomStatusJSON(res, chat, http.StatusCreated)
	})
}

// supported query funcs
// $contains(array of values) => []
// $before(date) => (row.datefield < date)
// $after(date) => (row.datefield > date)

func SearchChats(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    // queryParams, err := url.ParseQuery(req.URL.RawQuery)
    // if err != nil {
    //   http.Error(res, err.Error(), http.StatusBadRequest)
    //   return
    // }

    // title, ok := queryParams["title"]
    // members, ok := queryParams["members"]
    // owner, ok := queryParams["owner"]
    // createdAt, ok := queryParams["createdAt"]
    // lastMessageAt, ok := queryParams["lastMessageAt"]
    // typeOf, ok := queryParams["typeOf"]

    // chatId := pat.Param(req, "id")
    // messages, err := chatservice.GetAllMessagesByChatID(db, chatId)
    // if err != nil {
    //   http.Error(res, err.Error(), http.StatusInternalServerError)
    //   return
    // }
    //
		// httputils.ReturnJSON(res, messages)
    page, ok := httputils.AcceptPagination(req)
    if !ok {
      page.Page = 1
      page.Offset = 0
      page.PageSize = 20
    }

    chats, err := chatservice.SearchChats(db, page, nil)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

    httputils.ReturnJSON(res, chats)
	})
}

func DeleteChatByID(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		chatId := pat.Param(req, "id")
    err := chatservice.DeleteChatByID(db, chatId)
    if err != nil {
      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }
	})
}
