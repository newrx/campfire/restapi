package controller

import (
  "net/http"
  "fmt"
  "time"

  "goji.io/pat"

  "gitlab.com/newrx/campfire/api/service/log"
  "gitlab.com/newrx/campfire/api/service/database"
  "gitlab.com/newrx/campfire/api/service/userservice"
  "gitlab.com/newrx/campfire/api/service/s3"
)

func UploadUserAvatar(db *database.Database, bucket *s3.Bucket, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
	})
}

func DownloadUserAvatar(db *database.Database, bucket *s3.Bucket, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
    id := pat.Param(req, "id")
    file, err := userservice.GetAvatarByID(bucket, id)
    if err != nil {
      if err == s3.ErrNotFound {
        http.Error(res, "failed to retreive avatar: user not found", http.StatusNotFound)
        return
      }

      http.Error(res, err.Error(), http.StatusInternalServerError)
      return
    }

    http.ServeContent(res, req, fmt.Sprintf("%s.png", id), time.Now(), file)
	})
}

func UpdateUserPreferences(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
	})
}


func UpdateUserLocation(db *database.Database, logger log.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
	})
}
