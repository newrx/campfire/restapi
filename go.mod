module gitlab.com/newrx/campfire/api

go 1.12

require (
	github.com/dchest/siphash v1.2.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dgryski/go-identicon v0.0.0-20140725220403-371855927d74
	github.com/go-ini/ini v1.42.0 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/lib/pq v1.1.0
	github.com/minio/minio-go v6.0.14+incompatible
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/nullrocks/identicon v0.0.0-20180626043057-7875f45b0022
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pressly/goose v2.6.0+incompatible // indirect
	github.com/prometheus/client_golang v0.9.2
	github.com/rs/zerolog v1.13.0
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a // indirect
	github.com/ziutek/mymysql v1.5.4 // indirect
	goji.io v2.0.2+incompatible
	golang.org/x/crypto v0.0.0-20190417174047-f416ebab96af
	golang.org/x/net v0.0.0-20190415214537-1da14a5a36f2 // indirect
	google.golang.org/appengine v1.5.0 // indirect
	gopkg.in/ini.v1 v1.42.0 // indirect
)
