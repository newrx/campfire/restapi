CREATE TABLE signuptokens (
  phone_number VARCHAR(10) PRIMARY KEY,
  created_at TIMESTAMPTZ NOT NULL,
  verification_code VARCHAR(6) NOT NULL,
  verified BOOLEAN NOT NULL DEFAULT FALSE,
  jwt TEXT NOT NULL
);

CREATE TABLE authtokens (
  id UUID PRIMARY KEY,
  actor_id UUID NOT NULL,
  issuer VARCHAR(256) NOT NULL,
  issued_at TIMESTAMPTZ NOT NULL,
  revoked BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE users (
  id UUID PRIMARY KEY,
  username VARCHAR(256) NOT NULL UNIQUE,
  name VARCHAR(256) NOT NULL,
  bio VARCHAR(512) NOT NULL,
  phone_number VARCHAR(10) NOT NULL,
  created_at TIMESTAMPTZ NOT NULL,
  last_mod_at TIMESTAMPTZ NOT NULL
);

CREATE TABLE users_roles_lookup (
  user_id UUID NOT NULL,
  role_id UUID NOT NULL
);

CREATE TABLE roles (
  id VARCHAR(36) PRIMARY KEY,
  description VARCHAR(100) NOT NULL,
  created_at TIMESTAMPTZ NOT NULL
);

INSERT INTO roles (
  id, description, created_at
) VALUES (
  'userrole-sing-perm-nsms-signuptoken0', 'single-use signup token jwt for creating user accounts', NOW()
) ON CONFLICT DO NOTHING;


CREATE TABLE roles_permissions_lookup (
  role_id VARCHAR(36) NOT NULL,
  permission_name VARCHAR(36) DEFAULT 'GENERIC_PERMISSION',
  resource_name VARCHAR(36) NOT NULL,
  -- CREATE, READ, UPDATE, DELETE
  action_name VARCHAR(6) NOT NULL
);

INSERT INTO roles_permissions_lookup (
  role_id, permission_name, resource_name, action_name
) VALUES (
  'user-role-singleperm-sms-signuptoken', 'CREATE_USERACCOUNT', 'USER', 'CREATE'
) ON CONFLICT DO NOTHING;

CREATE TABLE user_preferences (
  user_id UUID PRIMARY KEY
);

CREATE TABLE chats (
  id UUID PRIMARY KEY,
  title VARCHAR(256) NOT NULL,
  created_at TIMESTAMPTZ NOT NULL
);

CREATE TABLE chats_owners_lookup (
  chat_id UUID NOT NULL,
  owner_user_id UUID NOT NULL
);

CREATE TABLE chats_last_messagetime_lookup (
  chat_id UUID NOT NULL,
  last_message_at TIMESTAMPTZ NOT NULL
);

CREATE TABLE chats_members_lookup (
  chat_id UUID NOT NULL,
  user_id UUID NOT NULL
);

CREATE TABLE chats_messages_lookup (
  chat_id UUID NOT NULL,
  message_id UUID NOT NULL
);

CREATE TABLE messages (
  id UUID PRIMARY KEY,
  -- custom mime types, supported:
  -- text/plain
  -- text/markdown
  -- application/json
  content_type VARCHAR(36) NOT NULL DEFAULT 'text/plain',
  body TEXT NOT NULL,
  sent_at TIMESTAMPTZ
);
