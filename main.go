package main

import (
	"fmt"
	"net/http"

	"gitlab.com/newrx/campfire/api/config"
	"gitlab.com/newrx/campfire/api/routes"
	"gitlab.com/newrx/campfire/api/service/log"
	"gitlab.com/newrx/campfire/api/service/s3"
	"gitlab.com/newrx/campfire/api/service/sms"
	"gitlab.com/newrx/campfire/api/service/geospatial"
	"gitlab.com/newrx/campfire/api/service/database"
	"gitlab.com/newrx/campfire/api/service/authservice"
)

func main() {
	conf, err := config.Fetch()
	if err != nil {
		panic(err)
	}

	logger := log.New(conf.Log.Pretty, conf.AppInfo.Name, conf.Log.Level)
	messenger := sms.New(sms.Config{
		APIKey: conf.Nexmo.APIKey,
		Secret: conf.Nexmo.Secret,
		SenderNumber: conf.Nexmo.SenderNumber,
	})

	db, err := database.Dial(database.Config{
		User: conf.PostgresDB.User,
		Password: conf.PostgresDB.Password,
		Host: conf.PostgresDB.Host,
		Port: conf.PostgresDB.Port,
		MigrationDir: conf.PostgresDB.MigrationDir,
		DatabaseName: conf.PostgresDB.DatabaseName,
	})
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to set up the database")
	}

	currentMigration, err := db.GetCurrentMigration()
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to fetch migration status")
	}
	logger.Info().
		Uint64("version", currentMigration.Version).
		Bool("complete", currentMigration.Complete).
		Str("hash", currentMigration.Hash).
		Msg("current database version determined")

	migrations, err := db.DiffMigrations()
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to diff the available migrations")
	}

	fmt.Println(migrations)

	status, err := db.RunMigrations(currentMigration, migrations...)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to execute migrations")
	}

	logger.Info().
		Uint64("applied", status.Applied).
		Uint64("failed", status.Failed).
		Uint64("skipped", status.Skipped).
		Uint64("version", status.Latest).
		Msg("ran database migrations")

	bucket, err := s3.New(s3.Config{
		Host: conf.S3.Host,
		APIKey: conf.S3.APIKey,
		Secret: conf.S3.Secret,
	})
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to dial the storage bucket provider")
	}

	geodb, err := geospatial.Dial(geospatial.Config{
		Host: conf.GeoDB.Host,
		User: conf.GeoDB.User,
		Password: conf.GeoDB.Password,
		Database: conf.GeoDB.Database,
	})
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to dial the geospatial database")
	}

	auth, err := authservice.New(
		fmt.Sprintf("%s.campfire.io", conf.Service.Env),
		fmt.Sprintf("%s.mobile.campfire.io", conf.Service.Env),
		conf.Service.SigningKey,
	)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to load authservice rsa keys")
	}

	logger.Info().Interface("config", conf).Msg("starting campfire api")
	go logger.Info().Str("address", conf.Service.ListenAddress).Msg("started campfire api http listener")
	http.ListenAndServe(conf.Service.ListenAddress, routes.Generate(auth, db, geodb, bucket, messenger, logger))
}
