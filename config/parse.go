package config

import (
  "github.com/kelseyhightower/envconfig"

  "gitlab.com/newrx/campfire/api/service/log"
)

type ValueMap struct{
  GeoDB struct{
    Host string `required:"true"`
    User string `required:"true"`
    Password string `required:"true"`
    Database int `default:"0"`
  } `envconfig:"GEO_DB" required:"true"`

  PostgresDB struct{
    User string `required:"true"`
    Password string `required:"true"`
    Host string `required:"true"`
    Port string `required:"true"`
    MigrationDir string `required:"true"`
    DatabaseName string `default:"campfire_api"`
  } `envconfig:"POSTGRES" required:"true"`

  S3 struct{
    Host string `required:"true"`
    APIKey string `required:"true"`
    Secret string `required:"true"`
  } `required:"true"`

  Nexmo struct{
    SenderNumber string `required:"true"`
    APIKey string `required:"true"`
    Secret string `required:"true"`
  } `required:"true"`

  AppInfo struct{
    Name string
  } `envconfig:"-"`

  Log struct{
    Pretty bool `envconfig:"-"`
    Level log.Level `default:"info"`
  }

  Service struct{
    Env string `required:"true"`
    ListenAddress string `default:"0.0.0.0:5000"`
    SigningKey string `required:"true" json:"-"`
  }
}

func parse() (ValueMap, error) {
  envVals := ValueMap{}
  err := envconfig.Process("capi", &envVals)
  if err != nil {
     return ValueMap{}, err
  }

  return envVals, nil
}
