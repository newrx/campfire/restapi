package config

import (
  "errors"
  "io/ioutil"
)

type Env string

const (
  LocalDev Env = "localdev"
  Integration Env = "integration"
  Secure Env = "secure"
)

func Fetch() (ValueMap, error) {
  conf, err :=  parse()
  if err != nil {
    return ValueMap{}, err
  }

  switch Env(conf.Service.Env) {
  case LocalDev:
    conf.Log.Pretty = true

  case Integration:
    conf.Log.Pretty = true

  case Secure:
    conf.Log.Pretty = false

  default:
    return ValueMap{}, errors.New("unrecognized environment, must be oneof localdev, integration or secure")
  }

  rsaKey, err := ioutil.ReadFile(conf.Service.SigningKey)
  if err != nil {
    return ValueMap{}, err
  }

  conf.Service.SigningKey = string(rsaKey)
  conf.AppInfo.Name = "campfire-api"

  return conf, nil
}
